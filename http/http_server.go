package http

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"net/http"
)

type Server struct {
	HttpServer *http.Server
}

func NewServer(config ConfigServerHttp, router *chi.Mux) *Server {
	server := &Server{}

	httpServer := &http.Server{
		Addr:              fmt.Sprintf(":%d", config.Port),
		WriteTimeout:      config.Timeout,
		ReadHeaderTimeout: config.Timeout,
		ReadTimeout:       config.Timeout,
		IdleTimeout:       config.IdleTimeout,
		Handler:           router,
	}

	server.HttpServer = httpServer

	return server
}

func (server Server) ListenAndServe() error {
	return server.HttpServer.ListenAndServe()
}
