Репозиторий, где собраны общие пакеты для наших гоу сервисов

Сгенерируйте Go код из файлов .proto с помощью компилятора protoc и плагина для Go:
```protoc -I grpc/proto/ grpc/proto/service.proto --go_out=./ --go-grpc_out=./```