package requestValidator

type Request interface {
	Validate() error
}
