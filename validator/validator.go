package requestValidator

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/schema"
	"gitlab.com/dev-tools-kit-bot/go-pkg/gologger"
	"golang.org/x/exp/slog"
	"net/http"
	"reflect"
	"strings"
)

const tagCustom = "error"

func Validate(r *http.Request, request Request) error {
	logger := gologger.GetInstance().With(
		slog.String("request_id", middleware.GetReqID(r.Context())),
	)

	var err error
	switch r.Method {
	case http.MethodPost:
		err = decodePost(r, request, logger)
	case http.MethodGet:
		err = decodeGet(r, request, logger)
	}

	if err != nil {
		return err
	}

	if err = request.Validate(); err != nil {
		return err
	}
	return nil
}

func decodePost(r *http.Request, request Request, logger *slog.Logger) error {
	err := render.DecodeJSON(r.Body, &request)
	if err != nil {
		logger.Error("failed to decode request body", err)

		if value, ok := err.(*json.UnmarshalTypeError); ok {
			return fmt.Errorf("field %s must be of type %s", value.Field, value.Type)
		}

		return fmt.Errorf("failed to decode request body: %s", err)
	}

	logger.Info("request body decoded", slog.Any("request", request))
	return nil
}

func decodeGet(r *http.Request, request Request, logger *slog.Logger) error {
	// Попытка декодировать параметры запроса в структуру
	err := schema.NewDecoder().Decode(request, r.URL.Query())
	if err != nil {
		return fmt.Errorf("invalid parameters: %s", err)
	}

	logger.Info("request params decoded", slog.Any("request", request))
	return nil
}

func ValidateFunc[T interface{}](obj interface{}) (errs error) {
	validate := validator.New()
	o := obj.(T)

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in Validate:", r)
			errs = fmt.Errorf("can't validate %+v", r)
		}
	}()

	if err := validate.Struct(o); err != nil {
		errorValid := err.(validator.ValidationErrors)
		for _, e := range errorValid {
			// snp  X.Y.Z
			snp := e.StructNamespace()
			errmgs := errorTagFunc[T](obj, snp, e.Field(), e.ActualTag())
			if errmgs != nil {
				errs = errors.Join(errs, fmt.Errorf("%w", errmgs))
			} else {
				errs = errors.Join(errs, fmt.Errorf("%w", e))
			}
		}
	}

	if errs != nil {
		return errs
	}

	return nil
}

func errorTagFunc[T interface{}](obj interface{}, snp string, fieldname, actualTag string) error {
	o := obj.(T)

	if !strings.Contains(snp, fieldname) {
		return nil
	}

	fieldArr := strings.Split(snp, ".")
	rsf := reflect.TypeOf(&o).Elem()

	for i := 1; i < len(fieldArr); i++ {
		field, found := rsf.FieldByName(fieldArr[i])
		if found {
			if fieldArr[i] == fieldname {
				customMessage := field.Tag.Get(tagCustom)
				if customMessage != "" {
					return fmt.Errorf("field '%s': %s (%s)", fieldname, customMessage, actualTag)
				}
				return fmt.Errorf("field '%s': field must be %s", fieldname, actualTag)
			} else {
				nestedFieldType := field.Type
				rsf = nestedFieldType
			}
		}
	}
	return nil
}
