package db

import (
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"log"
	"time"
)

type ConfigDB struct {
	Host               string `yaml:"host"`
	Port               int    `yaml:"port"`
	Name               string `yaml:"name"`
	User               string `yaml:"user"`
	Password           string `yaml:"password"`
	ConnectionString   string `yaml:"connection_string"`
	MaxOpenConnection  int    `yaml:"max_open_connection" env-default:"-1"`
	MaxIdleConnection  int    `yaml:"max_idle_connection" env-default:"-1"`
	MaxConnLifetimeSec int    `yaml:"max_conn_lifetime_sec" env-default:"-1"`
	SslMode            string `yaml:"ssl_mode" env-default:"disable"`
}

var (
	instance *sqlx.DB
	cfg      *ConfigDB
)

func registerConfig(config *ConfigDB) {
	cfg = config
}

func GetInstance() *sqlx.DB {
	return instance
}

func Open(cfg *ConfigDB) error {
	registerConfig(cfg)

	if instance != nil {
		return fmt.Errorf("db connection already opened")
	}

	if cfg == nil {
		return fmt.Errorf("check environment configurations")
	}

	if cfg.ConnectionString == "" {
		cfg.ConnectionString = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
			cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.Name, cfg.SslMode)
	}

	db, err := sqlx.Connect("pgx", cfg.ConnectionString)
	if err != nil {
		return fmt.Errorf("connect error: %s", err)
	}

	instance = db
	if cfg.MaxOpenConnection != -1 {
		instance.SetMaxOpenConns(cfg.MaxOpenConnection)
	}
	if cfg.MaxIdleConnection != -1 {
		instance.SetMaxIdleConns(cfg.MaxIdleConnection)
	}
	if cfg.MaxConnLifetimeSec != -1 {
		instance.SetConnMaxLifetime(time.Duration(cfg.MaxConnLifetimeSec) * time.Second)
	}

	return nil
}

func Migrate() error {
	driver, err := postgres.WithInstance(GetInstance().DB, &postgres.Config{})
	if err != nil {
		log.Fatal(err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://migrations", cfg.Name, driver)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}

func Close() {
	if instance != nil {
		_ = instance.Close()
	}
}
