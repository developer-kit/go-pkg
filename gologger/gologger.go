package gologger

import (
	"context"
	"golang.org/x/exp/slog"
	"os"
)

var logger *Gologger

type Gologger struct {
	slog *slog.Logger
}

func newLogger(slog *slog.Logger) *Gologger {
	logger = &Gologger{
		slog: slog,
	}

	return logger
}

func GetInstance() *Gologger {
	return logger
}

func (g *Gologger) Info(msg string, args ...any) {
	g.slog.Info(msg, args...)
}

func (g *Gologger) InfoContext(ctx context.Context, msg string, args ...any) {
	g.slog.DebugContext(ctx, msg, args...)
}

func (g *Gologger) Debug(msg string, args ...any) {
	g.slog.Debug(msg, args...)
}

func (g *Gologger) DebugContext(ctx context.Context, msg string, args ...any) {
	g.slog.DebugContext(ctx, msg, args...)
}

func (g *Gologger) Warn(msg string, args ...any) {
	g.slog.Warn(msg, args...)
}

func (g *Gologger) WarnContext(ctx context.Context, msg string, args ...any) {
	g.slog.WarnContext(ctx, msg, args...)
}

func (g *Gologger) Error(msg string, args ...any) {
	g.slog.Error(msg, args...)
}

func (g *Gologger) ErrorContext(ctx context.Context, msg string, args ...any) {
	g.slog.ErrorContext(ctx, msg, args...)
}

func (g *Gologger) FatalError(msg string, args ...any) {
	g.slog.Error(msg, args...)
	os.Exit(1)
}

func (g *Gologger) With(args ...any) *slog.Logger {
	return g.slog.With(args...)
}
