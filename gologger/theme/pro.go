package theme

import (
	"context"
	"encoding/json"
	"github.com/fatih/color"
	"golang.org/x/exp/slog"
	"io"
	stdLog "log"
)

type ProHandlerOptions struct {
	SlogOpts *slog.HandlerOptions
}

type ProHandler struct {
	opts ProHandlerOptions
	slog.Handler
	logger *stdLog.Logger
	attrs  []slog.Attr
}

func (opts ProHandlerOptions) NewProHandler(
	out io.Writer,
) *ProHandler {
	h := &ProHandler{
		Handler: slog.NewJSONHandler(out, opts.SlogOpts),
		logger:  stdLog.New(out, "", 0),
	}

	return h
}

func (handler *ProHandler) Handle(_ context.Context, r slog.Record) error {
	level := r.Level.String() + ":"

	switch r.Level {
	case slog.LevelDebug:
		level = color.MagentaString(level)
	case slog.LevelInfo:
		level = color.BlueString(level)
	case slog.LevelWarn:
		level = color.YellowString(level)
	case slog.LevelError:
		level = color.RedString(level)
	}

	fields := make(map[string]interface{}, r.NumAttrs())

	r.Attrs(func(a slog.Attr) bool {
		fields[a.Key] = a.Value.Any()

		return true
	})

	for _, a := range handler.attrs {
		fields[a.Key] = a.Value.Any()
	}

	var b []byte
	var err error

	if len(fields) > 0 {
		b, err = json.MarshalIndent(fields, "", "  ")
		if err != nil {
			return err
		}
	}

	timeStr := r.Time.Format("[15:05:05.000]")
	msg := color.CyanString(r.Message)

	handler.logger.Println(
		timeStr,
		level,
		msg,
		color.WhiteString(string(b)),
	)

	return nil
}

func (handler *ProHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &ProHandler{
		Handler: handler.Handler,
		logger:  handler.logger,
		attrs:   attrs,
	}
}

func (handler *ProHandler) WithGroup(name string) slog.Handler {
	return &ProHandler{
		Handler: handler.Handler.WithGroup(name),
		logger:  handler.logger,
	}
}
