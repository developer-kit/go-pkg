package gologger

import (
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/dev-tools-kit-bot/go-pkg/config"
	"gitlab.com/dev-tools-kit-bot/go-pkg/gologger/theme"
	"golang.org/x/exp/slog"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

func Load(config *config.Config) *Gologger {
	file, err := os.OpenFile(config.ServiceName+".log", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal(err)
	}

	//defer file.Close()

	switch config.Env {
	case envLocal:
		newLogger(loadProTheme())
	case envDev:
		newLogger(slog.New(slog.NewJSONHandler(file, &slog.HandlerOptions{Level: slog.LevelDebug})))
	case envProd:
		newLogger(slog.New(slog.NewJSONHandler(file, &slog.HandlerOptions{Level: slog.LevelInfo})))
	default: // If env config is invalid, set prod settings by default due to security
		newLogger(slog.New(slog.NewJSONHandler(file, &slog.HandlerOptions{Level: slog.LevelInfo})))
	}

	return logger
}

func loadProTheme() *slog.Logger {
	opts := theme.ProHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}

	handler := opts.NewProHandler(os.Stdout)

	return slog.New(handler)
}

func LoggerMiddleware() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		logger.Info("logger middleware enabled")

		fn := func(w http.ResponseWriter, r *http.Request) {
			entry := logger.With(
				slog.String("method", r.Method),
				slog.String("path", r.URL.Path),
				slog.String("remote_addr", r.RemoteAddr),
				slog.String("user_agent", r.UserAgent()),
				slog.String("request_id", middleware.GetReqID(r.Context())),
			)
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			t1 := time.Now()
			defer func() {
				entry.Info("request completed",
					slog.Int("status", ww.Status()),
					slog.Int("bytes", ww.BytesWritten()),
					slog.String("duration", time.Since(t1).String()),
				)
			}()

			next.ServeHTTP(ww, r)
		}

		return http.HandlerFunc(fn)
	}
}
