package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/dev-tools-kit-bot/go-pkg/bot"
	"gitlab.com/dev-tools-kit-bot/go-pkg/db"
	"gitlab.com/dev-tools-kit-bot/go-pkg/grpc"
	"gitlab.com/dev-tools-kit-bot/go-pkg/http"
	"log"
	"os"
)

type Config struct {
	ServiceName             string `yaml:"service_name"`
	Env                     string `yaml:"env" env-default:"dev"`
	db.ConfigDB             `yaml:"database"`
	http.ConfigServerHttp   `yaml:"http_server"`
	grpc.ConfigServerGRPC   `yaml:"grpc_server"`
	grpc.ConfigGrpcServices `yaml:"grpc_services"`
	bot.ConfigBot           `yaml:"bot"`
}

func Load() (*Config, error) {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "./config/app.yaml"
	}

	// check if file exists
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Fatalf("config file does not exist: %s", configPath)
	}

	var cfg Config

	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		log.Fatalf("cannot read config: %s", err)
	}

	return &cfg, nil
}
