package api

import (
	"encoding/json"
	"errors"
	"gitlab.com/dev-tools-kit-bot/go-pkg/gologger"
	"net/http"
	"strings"
	"syscall"
)

type Error struct {
	ErrorMessage string
	StatusCode   int
	ErrorCode    int
	ErrorData    map[string]interface{}
}

type ErrorResponse struct {
	Success bool        `json:"success"`
	Result  ErrorResult `json:"result"`
}

type SuccessResponse struct {
	Success bool        `json:"success"`
	Result  interface{} `json:"result"`
}

type ErrorResult struct {
	Error string                 `json:"error"`
	Code  int                    `json:"code"`
	Data  map[string]interface{} `json:"data,omitempty"`
}

func SendResponse(w http.ResponseWriter, r *http.Request, response interface{}) {
	result := SuccessResponse{Success: true, Result: response}
	js, _ := json.Marshal(result)

	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(js)
	if err != nil {
		handleWriteError(err)
	}
}

func handleWriteError(err error) {
	if errors.Is(err, syscall.EPIPE) {
		// The connection was forcibly closed in the client side
		return
	}

	logger := gologger.GetInstance()
	logger.Error("Failed send response")
}

func SendError(w http.ResponseWriter, r *http.Request, error *Error) {
	logger := gologger.GetInstance()

	w.Header().Set("Content-Type", "application/json")
	if error.StatusCode == 0 {
		error.StatusCode = http.StatusInternalServerError
	}
	w.WriteHeader(error.StatusCode)
	errorMsg := error.ErrorMessage
	errorCode := error.ErrorCode
	if errorCode == 0 {
		errorCode = error.StatusCode
	}

	switch error.StatusCode {
	case http.StatusInternalServerError:
		if errorCode != 800 {
			errorMsg = "Internal server error"
		}
	case http.StatusUnauthorized:
		errorMsg = "Your request was made with invalid credentials."
	}
	logger.Error(error.ErrorMessage)

	result := ErrorResult{
		Error: strings.ToLower(errorMsg),
		Code:  errorCode,
	}
	if error.ErrorData != nil {
		result.Data = error.ErrorData
	}

	apiError := ErrorResponse{
		Success: false,
		Result:  result,
	}
	js, _ := json.Marshal(apiError)
	_, err := w.Write(js)
	if err != nil {
		handleWriteError(err)
	}
	return
}
