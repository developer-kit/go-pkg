package grpc

type ConfigServerGRPC struct {
	Port int `yaml:"port"`
}

type ConfigGrpcServices struct {
	ConfigUserService ConfigService `yaml:"user_service"`
}

type ConfigService struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}
