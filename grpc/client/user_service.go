package grpc_client

import (
	"gitlab.com/dev-tools-kit-bot/go-pkg/grpc"
	google "google.golang.org/grpc"
	"log"
)

var userServiceInstance *UserService

const UserServiceName = "userService"

type UserService struct {
	Config grpc.ConfigService
}

// OpenConnection @todo подумать над реализацией текущего функционала
func (s *UserService) OpenConnection() error {
	mu.Lock()
	defer mu.Unlock()

	userServiceInstance = s

	// Сохраняем соединение
	conn, err := Deal(s.Config.Host, s.Config.Port)
	connections[UserServiceName] = ConnectionInfo{connection: conn}

	return err
}

func GetUserServiceInstance() *UserService {
	if userServiceInstance == nil {
		log.Fatalf("There are not opened connections for: %s", UserServiceName)
	}
	return userServiceInstance
}

func (s UserService) GetConnection() *google.ClientConn {
	return connections[UserServiceName].connection
}

func (s *UserService) Close() {
	if connections[UserServiceName].connection != nil {
		_ = connections[UserServiceName].connection.Close()
	}
}
