package grpc_client

import (
	"fmt"
	"gitlab.com/dev-tools-kit-bot/go-pkg/grpc"
	google "google.golang.org/grpc"
	"sync"
)

var (
	connections = make(map[string]ConnectionInfo)
	mu          sync.Mutex
)

type ConnectionInfo struct {
	connection *google.ClientConn
}

type Client struct {
	cfg *grpc.ConfigGrpcServices
}

func NewClient(config *grpc.ConfigGrpcServices) *Client {
	return &Client{
		cfg: config,
	}
}

func Deal(host string, port int) (*google.ClientConn, error) {
	conn, err := google.Dial(fmt.Sprintf("%s:%d", host, port), google.WithInsecure())
	return conn, err
}

func Close() {
	for _, conn := range connections {
		_ = conn.connection.Close()
	}
}

func (c *Client) UserService() *UserService {
	return &UserService{
		Config: c.cfg.ConfigUserService,
	}
}
