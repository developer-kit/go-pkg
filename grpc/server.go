package grpc

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
)

type Server struct {
	port     int
	instance *grpc.Server
}

func NewServer(config ConfigServerGRPC) *Server {
	return &Server{
		port:     config.Port,
		instance: grpc.NewServer(),
	}
}

func (server *Server) Start() error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", server.port))
	if err != nil {
		return err
	}

	if err = server.instance.Serve(lis); err != nil {
		return err
	}

	return nil
}

func (server *Server) GetInstance() *grpc.Server {
	return server.instance
}

func (server *Server) Stop() {
	if server.instance != nil {
		server.instance.GracefulStop()
	}
}
